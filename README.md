# Jeu des allumettes 


### Description
Ce jeu se joue à deux. Choisissez le nombre d'allumettes de départ. Tour à tour, chaque joueur prend un nombre d'allumettes compris entre 1 et 3. Le gagnant est celui qui prend la dernière allumette.
Bonne Chance !


### Usage
Pour commencer, choisissez un nombre d'allumettes, puis commencer.


### Deroulement
Chaque joueur choisis le nombre d'allumettes qu'il veut retirer, entre une à trois allumettes.


### Fin
Le gagnant sera celui qui aura retirer la dernière allumettes.
